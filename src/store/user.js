import * as fb from "firebase";
class User {
  constructor(id) {
    this.id = id;
  }
}

export default {
  state: {
    authInit: false,
    user: null,
    userLoggedIn: false,
    usersInfo: []
  },
  mutations: {
    clearError() {},
    setAuthInit(state, payload) {
      state.authInit = payload;
    },
    setUser(state, payload) {
      state.user = payload;
    },
    loadUsersInfo(state, payload) {
      state.usersInfo = payload;
    },

    setLoggedIn(state, userLoggedIn) {
      state.userLoggedIn = userLoggedIn;
    }
  },
  actions: {
    async registerUser(
      { commit },
      {
        name,
        surname,
        lastname,
        phone,
        email,
        card,
        passport,
        taxNumber,
        password,
        money
      }
    ) {
      // eslint-disable-next-line no-useless-catch
      try {
        const user = await fb
          .auth()
          .createUserWithEmailAndPassword(email, password);
        commit("setUser", new User(user.uid));
        await fb
          .database()
          .ref("users")
          .child(user.user.uid)
          .set({
            name: name,
            surname: surname,
            lastname: lastname,
            phone: phone,
            email: email,
            card: card,
            passport: passport,
            taxNumber: taxNumber,
            money: money
          });
      } catch (error) {
        throw error;
      }
    },
    async loginUser({ commit }, { email, password }) {
      // eslint-disable-next-line no-useless-catch
      try {
        const user = await fb
          .auth()
          .signInWithEmailAndPassword(email, password);
        commit("setUser", new User(user.uid));
        location.reload();
      } catch (error) {
        throw error;
      }
    },
    autoLoginUser({ commit }, payload) {
      commit("setUser", new User(payload.uid));
      /* AuthGuard */
    },
    logoutUser({ commit }) {
      fb.auth().signOut();
      commit("setUser", null);
    },
    async fetchUserInfo({ commit }) {
      // eslint-disable-next-line no-useless-catch
      try {
        const fbVal = await fb
          .database()
          .ref("users")
          .once("value");
        const usersInfo = fbVal.val();
        commit("loadUsersInfo", usersInfo);
      } catch (error) {
        throw error;
      }
    },
    async switchLoggedIn(context, userLoggedIn) {
      context.commit("setLoggedIn", userLoggedIn);
    },
    async switchAuthInit(context, authInit) {
      context.commit("setAuthInit", authInit);
    },
    async updateMoney({ commit }, payload) {
      fb.database()
        .ref("users")
        .child(localStorage.getItem("userId"))
        .update({ money: payload });
      commit("loadUsersInfo", payload);
    }
  },
  getters: {
    usersInfo(state) {
      return state.usersInfo;
    },
    isUserLoggedIn(state) {
      return state.userLoggedIn;
    },
    isAuthInit(state) {
      return state.authInit;
    },
    user(state) {
      return state.user;
    }
  }
};
