import firebase from "firebase";
import store from "../store";
import EventBus from "../components/Utils/event-bus";

export default firebase.initializeApp({
  apiKey: "AIzaSyAGUuD8oDf1Z0RTKYoWTwdL3LxJjClWnBo",
  authDomain: "fntech-de006.firebaseapp.com",
  databaseURL: "https://fntech-de006.firebaseio.com",
  projectId: "fntech-de006",
  storageBucket: "fntech-de006.appspot.com",
  messagingSenderId: "566316016274",
  appId: "1:566316016274:web:b9b9176c4d12fe16e9bc4b"
});

firebase.auth().onAuthStateChanged(userAuth => {
  EventBus.$emit("userLoggedIn", false);
  if (userAuth) {
    EventBus.$emit("userLoggedIn", true);
    firebase
      .auth()
      .currentUser.getIdTokenResult()
      .then(tokenResult => {
        localStorage.setItem("userId", tokenResult.claims.user_id);
        store.dispatch("switchAuthInit", true);
      });
  } else {
    EventBus.$emit("userLoggedIn", false);
    store.dispatch("switchAuthInit", true);
  }
});
