import Vue from "vue";
import VueRouter from "vue-router";
import Auth from "../components/User/Auth";
import Registartion from "../components/User/Reg/Registartion";
import Main from "../components/Main/Main";
import Send from "../components/Main/Send";
// import store from "../store";

Vue.use(VueRouter);
// const CheckUser = (to, from, next) => {
//   if (store.getters.isUserLoggedIn) {
//     next();
//   } else {
//     next("/auth");
//   }
// };

const routes = [
  {
    path: "/auth",
    name: "Auth",
    component: Auth
  },
  {
    path: "/main",
    name: "Main",
    component: Main
    // beforeEnter: CheckUser
  },
  {
    path: "/send",
    name: "Send",
    component: Send
    // beforeEnter: CheckUser
  },
  {
    path: "/reg/:st?",
    props: true,
    name: "Reg",
    component: Registartion
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
