import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import fbInit from "./auth/fbInit";
import VueI18n from "vue-i18n";

Vue.use(VueI18n);

Vue.prototype.$userLogged = () => store.getters.isUserLoggedIn;
Vue.config.productionTip = false;
const messages = {
  ru: {
    main: {
      header: "Главная страница"
    },
    auth: {
        header: 'Авторизация'
    }
  }
};
const i18n = new VueI18n({
  locale: "ru", // set locale
  messages

  // set locale messages
});

new Vue({
  i18n,
  fbInit,
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
